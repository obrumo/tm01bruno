﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Quiz
{
    public string texto;
}

public class ClassResposta : Quiz
{
    public bool ecorreta;
    
    public ClassResposta(string a, bool b)
    {
        this.texto = a;
        this.ecorreta = b;
    }
}

public class ClassPergunta : Quiz
{ // CLASS PERGUNTAS
    public int pontos;
    public ClassResposta[] respostas;

    // METODO É PARA CRIAR AS PERGUNTAS
    public ClassPergunta(string t, int p)
    {
        this.texto = t;
        this.pontos = p;
    }

    // METODO PARA ADD RESPOSTA
    public void AddResposta(string a, bool b)
    {
        if (respostas == null)
        {
            respostas = new ClassResposta[4];
        }

        for (int i = 0; i < respostas.Length; i++)
        {
            if (respostas[i] == null)
            {
                respostas[i] = new ClassResposta(a, b);
                break;
            }
        }
    }

    // LISTA ou EXIBIR as respostas
    public void ListarRespostas()
    {
        if (respostas != null && respostas.Length > 0)
        {
            foreach (ClassResposta r in respostas)
            {
                Debug.Log(r);
            }
        }
    }

    // Checar resposta correta
    public bool ChecarRespostaCorreta(int pos)
    {
        if (respostas != null && respostas.Length > pos && pos >= 0)
        {
            return respostas[pos].ecorreta;
        }
        else
        {
            return false;
        }
    }

} // FIM CLASS PERGUNTAS


// CLASS LÓGICA - PRINCIPAL
public class Logica : MonoBehaviour
{ // INICIO CLASS LOGICA

    public Text tituloPergunta;
    public Button[] respostaBtn = new Button[4];
    public List<ClassPergunta> PerguntasLista = new List<ClassPergunta>();

    private ClassPergunta perguntaAtual;

    public GameObject PainelCorreto;
    public GameObject PainelErrado;
    public GameObject PainelFinal;
    private int contagemPerguntas = 0;
    private float tempo;
    private bool carregarProximaPergunta;
    private int pontos = 0;


    void Start()
    { // METODO START

        ClassPergunta p1 = new ClassPergunta("1. Chernobyl e Césio-137 fazem parte dos maiores acidentes nucleares da história. Em que países aconteceram? ", 10);
        p1.AddResposta("Rússia e Espanha", false);
        p1.AddResposta("Ucrânia e Brasil", true);
        p1.AddResposta("Estados Unidos e Ucrânia", false);
        p1.AddResposta("Japão e Brasil", false);

        ClassPergunta p2 = new ClassPergunta("2.Qual a primeira mulher a ganhar um prêmio Nobel?", 10);
        p2.AddResposta("Elizabeth Blackweel", false);
        p2.AddResposta("Irène Joliot-Curie", false);
        p2.AddResposta("Marie Curie", true);
        p2.AddResposta("Valentina Tereshkova", false);

        ClassPergunta p3 = new ClassPergunta("3. Quais os quatros países que têm a maior população carcerária do mundo?", 10);
        p3.AddResposta("Estados Unidos, China, Rússia e Brasil", true);
        p3.AddResposta("Brasil, Estados Unidos, México e Índia", false);
        p3.AddResposta("China, Estados Unidos, Índia e Indonésia", false);
        p3.AddResposta("Rússia, Japão, Canadá e China", false);

        ClassPergunta p4 = new ClassPergunta("4. Qual empresa abaixo é brasileira?", 10);
        p4.AddResposta("Nestlé.", false);
        p4.AddResposta("Coca-Cola.", false);
        p4.AddResposta("Positivo.", true);
        p4.AddResposta("Fiat.", false);

        ClassPergunta p5 = new ClassPergunta("5. Marque a alternativa que está ERRADA quanto a Globalização:", 10);
        p5.AddResposta("A globalização trouxe muita desigualdade social.", false);
        p5.AddResposta("A taxa de morte vem cada vez mais aumentando.", true);
        p5.AddResposta("A base da globalização é o capital.", false);
        p5.AddResposta("Não esta presente em todo lugar do mundo.", false);

        ClassPergunta p6 = new ClassPergunta("6. Quantos partidos governam Cuba?", 10);
        p6.AddResposta("Um", true);
        p6.AddResposta("Dois", false);
        p6.AddResposta("Nenhuma das anteriores", false);
        p6.AddResposta("O regime é pluripartidário, como no Brasil", false);

        ClassPergunta p7 = new ClassPergunta("7. Ganhador do prêmio Nobel da Paz, Martin Luther King fez um discurso histórico em Washington há 50 anos. Qual era a luta dele?", 10);
        p7.AddResposta("Por cotas raciais nas universidades dos EUA", false);
        p7.AddResposta("Pelo direito de voto para a população negra", false);
        p7.AddResposta("Pela igualdade de direitos civis para brancos e negros", true);
        p7.AddResposta("Por salários mais justos para os negros", false);

        ClassPergunta p8 = new ClassPergunta("8. O programa Mais Médicos do Governo Federal trouxe profissionais estrangeiros de qual Pais?", 10);
        p8.AddResposta("Chile", false);
        p8.AddResposta("México", false);
        p8.AddResposta("Cuba", true);
        p8.AddResposta("Argentina", false);

        ClassPergunta p9 = new ClassPergunta("9. A deputada Marielle Franco foi assassinada em 2018 no Rio de Janeiro. Qual era o seu partido político?", 10);
        p9.AddResposta("PSL", false);
        p9.AddResposta("PDT", false);
        p9.AddResposta("PT", false);
        p9.AddResposta("PSOL", true);

        ClassPergunta p10 = new ClassPergunta("10. Em qual país está acontecendo a Copa do Mundo Feminina - 2019?", 10);
        p10.AddResposta("Italia", false);
        p10.AddResposta("Bolívia", false);
        p10.AddResposta("Holanda", false);
        p10.AddResposta("França", true);



        PerguntasLista.Add(p1);
        PerguntasLista.Add(p2);
        PerguntasLista.Add(p3);
        PerguntasLista.Add(p4);
        PerguntasLista.Add(p5);
        PerguntasLista.Add(p6);
        PerguntasLista.Add(p7);
        PerguntasLista.Add(p8);
        PerguntasLista.Add(p9);
        PerguntasLista.Add(p10);

        perguntaAtual = p1;

        ExibirPerguntasNoQuiz();


    } // FIM METODO START

    void Update()
    {
        if (carregarProximaPergunta == true)
        {
            tempo += Time.deltaTime;
            if (tempo > 3)
            {
                tempo = 1;
                carregarProximaPergunta = false;
                ProximaPergunta();
            }
        }
    }


    private void ExibirPerguntasNoQuiz()
    {
        tituloPergunta.text = perguntaAtual.texto;
        respostaBtn[0].GetComponentInChildren<Text>().text = perguntaAtual.respostas[0].texto;
        respostaBtn[1].GetComponentInChildren<Text>().text = perguntaAtual.respostas[1].texto;
        respostaBtn[2].GetComponentInChildren<Text>().text = perguntaAtual.respostas[2].texto;
        respostaBtn[3].GetComponentInChildren<Text>().text = perguntaAtual.respostas[3].texto;
    }

    public void ProximaPergunta()
    {
        contagemPerguntas++;
        if (contagemPerguntas < PerguntasLista.Count)
        {
            perguntaAtual = PerguntasLista[contagemPerguntas];
            ExibirPerguntasNoQuiz();
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
        }
        else
        {
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
            PainelFinal.SetActive(true);
            PainelFinal.transform.GetComponentInChildren<Text>().text = "Pontos: " + pontos.ToString();
        }
    }

    public void ClickResposta(int pos)
    {
        if (perguntaAtual.respostas[pos].ecorreta)
        {
            //Debug.Log("Resposta certa!!!");
            PainelCorreto.SetActive(true);
            pontos += perguntaAtual.pontos;
                
        }
        else
        {
            //Debug.Log("Resposta errada!!!");
            PainelErrado.SetActive(true);
        }

        carregarProximaPergunta = true;
    }

    public void IniciarQuiz()
    {
        SceneManager.LoadScene("Quiz");
    }

    public void MenuInicial()
    {
        SceneManager.LoadScene("MenuInicial");
    }

    public void Credito()
    {
        SceneManager.LoadScene("Credito");
    }

    public void SairJogo()
    {
        Application.Quit();
    }




}// FIM CLASS LOGICA
